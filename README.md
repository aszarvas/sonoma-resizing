# Problem description

We have observed serious performance degradation on MacOS Sonoma during the resizing of windows.

The code example we are providing in this repository
* sets the `drawsAsynchronously` property of `CALayer` to `YES`, and
* draws a complex path using `CGContextFillPath`, that is still simple enough that can be drawn at screen refresh rate.

When you launch the application on MacOS Sonoma, and then grab the edge of the window for resizing, the window's size changes in a very jittery fashion, with 100s of milliseconds in between each size update.

When running the application under the System Trace tool of Instruments, we can see that during resizing there is a continuous series of +300ms microhangs, while the Main Thread remains in a blocked state. The screenshot below shows the Main Thread's stack trace during one of these blocked intervals.

![System Trace for SonomaResizing app](system_trace_screenshot.png)

## The expected behaviour

The window's size should smoothly follow the mouse cursor's movement, barely lagging behind the cursor's position. This is the behaviour we can see up until Sonoma.
