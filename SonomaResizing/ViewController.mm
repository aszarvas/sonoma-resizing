//
//  ViewController.m
//  SonomaResizing
//
//  Created by Attila Szarvas on 02/10/2023.
//

#import "ViewController.h"

#include "TestPoints.h"

#include <vector>
#include <optional>

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
}


- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}


@end

//==============================================================================

struct JPoint
{
    float x{}, y{};
};

static auto& getTestPoints()
{
    static const auto points = []
    {
        std::vector<JPoint> result;
        
        for (size_t i = 0; i < numTestPathPairs; ++i)
        {
            JPoint point;
            
            point.x = testPath[2 * i];
            point.y = testPath[2 * i + 1];
            
            result.push_back (point);
        }
        
        return result;
    }();
    
    return points;
}

struct PathHelper
{
    PathHelper (CGContextRef contextIn, CGFloat heightIn)
        : context (contextIn),
          height (heightIn)
    {
        CGContextBeginPath (context);
    }
    
    void add (JPoint p, float scale = 1.0f)
    {
        if (! startingPoint.has_value())
        {
            startingPoint = p;
            CGContextMoveToPoint (context, scale * p.x, height - (scale * p.y));
            return;
        }
        
        CGContextAddLineToPoint (context, scale * p.x, height - (scale * p.y));
    }
    
    void fill()
    {
        CGContextClosePath (context);
        CGContextFillPath (context);
    }
    
private:
    CGContextRef context;
    std::optional<JPoint> startingPoint;
    CGFloat height;
};

static float getChangingScale()
{
    static float s = 1.0f;

    static bool breatheIn = true;

    if (breatheIn)
        s += 0.005f;
    else
        s -= 0.005f;

    if (s > 1.1f)
        breatheIn = false;
    else if (s < 0.9f)
        breatheIn = true;

    return s;
}

@implementation ResizingView

- (void)awakeFromNib {
    [super awakeFromNib];

    [self setWantsLayer: YES];
    [self setLayerContentsRedrawPolicy: NSViewLayerContentsRedrawDuringViewResize];
    [self layer].drawsAsynchronously = YES;

    [NSTimer scheduledTimerWithTimeInterval:0.0333
                                     target:self
                                   selector:@selector(redrawView)
                                   userInfo:nil
                                    repeats:YES];
}

- (void)redrawView {
    [self setNeedsDisplay:YES];
}

- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];

    NSColor *backgroundColor = [NSColor colorWithCalibratedRed:0.8 green:0.8 blue:0.8 alpha:1.0];
    [backgroundColor setFill];
    NSRectFill(dirtyRect);
    
    NSGraphicsContext *context = [NSGraphicsContext currentContext];
    CGContextRef cgContext = [context CGContext];
    
    PathHelper maker { cgContext, NSHeight (self.bounds) };
    
    auto& points = getTestPoints();
    
    const auto scale = getChangingScale();

    for (auto p : points)
        maker.add (p, scale);
    
    CGContextSetFillColorWithColor (cgContext, [[NSColor orangeColor] CGColor]);
    CGContextSetAlpha (cgContext, 1.0f);
    maker.fill();
}

@end
